import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import SearchWrapper from './components/search_wrapper';
import ProductList from './components/product_list';
import '../style/style.sass';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      autocomplete: [],
      products: [],
      term: '',
      showMe: false
    };
    this.componentDidMount('blank');
    // const term = this.state.term;
  }

  componentDidMount(term) {
    fetch(`http://agenciabluefoot.vtexcommercestable.com.br/api/catalog_system/pub/products/search/${term}?map=ft`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            products: result
          });
          // console.log(result)
        }
      )

    fetch(`http://agenciabluefoot.vtexcommercestable.com.br/buscaautocomplete/?productNameContains=${term}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            autocomplete: result.itemsReturned
          });
          // console.log(this.state.autocomplete)
        }
      )
  }

  changeState(term) {
    this.setState({term});
  }

  changeDisplay(boolean) {
    this.setState({showMe: boolean});
  }


  render() {
    if(this.state.showMe) {
      return (
        <div>
          <SearchBar changeDisplay={this.changeDisplay.bind(this)} changeState={this.changeState.bind(this)} term={this.state.term} onSearchTermChange={term => this.componentDidMount(term)} />
          <ProductList products={this.state.products} />
        </div>
      );
    } else {
      return (
        <div>
          <SearchBar changeDisplay={this.changeDisplay.bind(this)} changeState={this.changeState.bind(this)} term={this.state.term} onSearchTermChange={term => this.componentDidMount(term)} />
          <SearchWrapper changeState={this.changeState.bind(this)} term={this.state.term} products={this.state.products} words={this.state.autocomplete} />
        </div>
      );
    }
  }

}

ReactDOM.render(<App />, document.querySelector('.container'));



// onSearchTermChange={term => this.componentDidMount(term)}
