import React from 'react';
import ProductListItem from './product_list_item';

const ProductList = (props) => {

  const productListItem = props.products.map((product) => {
    return <ProductListItem key={product.productId} product={product} />
  });

  return (
    <ul className="product_list">
      {productListItem}
    </ul>
  );
}

export default ProductList;
