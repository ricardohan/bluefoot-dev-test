import React from 'react';

const ProductListItem = ({product}) => {

  const image = product.items[0].images[0].imageUrl;
  // console.log(product)

  return (

      <div className="product_list_container">
        <li className="">
          <img className="list_img" src={image} alt="product image"/>
          <div className="product_details">
            <h1>{product.productName}</h1>
            <h2>{product.brand}</h2>
            <h3 className="">R$ {product.items[0].sellers[0].commertialOffer.Price}</h3>
            <h4>{product.items[0].sellers[0].commertialOffer.AvailableQuantity} produtos disponíveis</h4>
            <a href={product.link}>Veja mais</a>
          </div>
        </li>
      </div>

  )
};

export default ProductListItem;
