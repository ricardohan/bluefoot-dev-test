import React from 'react';

const AutoCompleteList = (props) => {

  // console.log(props.term)
  // const name = props.word.name;
 const onClickChange = (name) => {
   props.changeState(name)
 }

  return (
    <li className="list">
      <h1 onClick={() => onClickChange(props.word.name)}>{props.word.name}</h1>
    </li>
  )
};

export default AutoCompleteList;
