import React from 'react';

const SearchProductItem = ({product}) => {

  const image = product.items[0].images[0].imageUrl;
  // console.log(product)

  return (
    <a className="search_product_link" href={product.items[0].sellers[0].addToCartLink}>
      <div className="search_product_container">
        <li className="search_product_single">
          <img className="search_img" src={image} alt="product image"/>
          <div className="search_detail">
            <h1>{product.productName}</h1>
            <h2>{product.brand}</h2>
            <h3 className="search_price">R$ {product.items[0].sellers[0].commertialOffer.Price}</h3>
            <h3>{product.items[0].sellers[0].commertialOffer.AvailableQuantity} produtos disponíveis</h3>
          </div>
        </li>
      </div>
    </a>
  )
};

export default SearchProductItem;
