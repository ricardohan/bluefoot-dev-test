# Bluefoot DEV: Teste prático para Frontend Developer

## O projeto

Este projeto utiliza ReactJs e SASS para criar uma interface de busca baseado nos produtos existentes na API da Bluefoot.
Além disso, esta aplicação conta com um mecanismo de 'autocomplete', ou seja, recomendações de busca baseadas nas palavras que o usuário está escrevendo. Esta ferramenta utiliza outra API para dar recomendações 'on the fly'.

## Como funciona

Ao começar a digitar qualquer palavra que corresponda à um produto do banco de dados, um container irá aparecer logo abaixo da área de input. Do lado esquerdo estão as palavras recomendadas e ao clicá-las, a área de input será preenchida automaticamente. Do lado direito irão aparecer os produtos recomendados e ao clicá-los, o usuário será diretamente encaminhado ao link do produto.

Quando decidir procurar por mais produtos que correspondem ao termo digitado, aperte o botão 'search' para ver todos os produtos serem retornados em uma lista. Cada produto exibido conta com um botão que encaminha diretamente ao link do produto.

## Organização e estrutura do código

Nesta aplicação foram usados 7 componentes de React ao todo. O componente 'index.js' é o parent e é nele que armazenamos todos os states e definimos métodos importantes para posteriormente passá-los aos outros componentes. Também é neste arquivo que pegamos e armazenamos os dados das API's, usando 'fetch'.

'search_wrapper.js' guarda o componente intermediário, que faz a ligação entre o componente pai e os componentes de nível mais baixo. Neste arquivo, pegamos as informações 'brutas' da API e tratamos ela de acordo com as necessidades dos componentes filhos.

Os componentes filhos em 'search_product_item' e 'autocompleteList' pegam essas informações da API através do 'props' e retornam JSX para exibir essas informações na tela.

Os componentes em 'product_list' e 'product_list_item' cuidam da lista de produtos que são exibidos quando o usuário aperta o botão de search. Eles seguem a mesma lógica dos componentes anteriores, onde 'product_list' funciona como componente intermediário e 'product_list_item' é o filho que retorna JSX com as informações recebidas através de props.

## Como rodar

Um detalhe importante que deve ser mencionado logo no início: aparentemente o banco de dados que armazena os produtos retornados pela API não permite acesso livre ao seu conteúdo. Por isso, usuários podem se deparar com o CORS error ao tentar usar a aplicação. Há vários modos de solucionar este erro, porém aqui decidi usar um plugin disponível para o Chrome, chamado "Access-Control-Allow-Origin: * "

Esta, sem dúvida nenhuma, não é a solução ideal pois é inviável exigir que todos os usuários instalem este plugin em seus dispositivos. Continuarei investigando a fim de encontrar uma solução melhor para esta aplicação.

Agora sim, podemos começar com as instruções de como rodar o projeto:

* Faça o download ou clone este projeto no seu computador. Você pode usar seu terminal ou baixar um arquivo ZIP diretamente do repositório

* Agora que você tem o projeto no seu computador, vá para o diretório do projeto (usando seu terminal, digite cd *endereço do projeto*) e use o comando *npm install*. Esse comando vai instalar as dependências necessárias para rodar o projeto e pode demorar alguns minutos para ser concluído

* Está quase tudo pronto para rodar, você só tem que rodar o comando *npm start* agora

* Ao fazer isso você abrirá um servidor local, provavelmente no PORT:8080 (confira seu terminal, lá estará escrito qual PORT o projeto foi lançado). Agora você só precisa abrir qualquer browser e digitar 'localhost:8080'

* Se tudo der certo, você verá a aplicação rodando normalmente! :+1:

* **Atenção ao CORS error**. Se nada aparecer na tela, abra o console do browser (clique com o botão direito em qualquer lugar, vá em 'inspecionar' e clique na aba 'console'). Caso você veja a mensagem *No Access-Control-Allow-Origin*, será necessário instalar o plugin "Access-Control-Allow-Origin: * ", como dito anteriormente.

## Aprendizado

Sendo sincero, não foi um desafio fácil de ser cumprido. Com certeza o modo como a aplicação foi criada não é a melhor de todas, a qualidade do código pode melhorar e a arquitetura do projeto também pode ser repensada para ser mais inteligente e menos desgastante.
Porém as lições e aprendizados que tive fazendo esta aplicação são imensuráveis. Com certeza levo adiante todo o conhecimento que adquiri durante este período. A seguir, aproveito para listar algumas lições importantes que me fizeram evoluir como desenvolvedor:  

* Percebi o quão importante é pensar na estrutura e organização do projeto quando se trabalha com componentes no React

* Pude praticar os conceitos de 'props' e 'state'

* Aprendi muito sobre como trabalhar com API's em React

* Resiliência: houveram momentos em que realmente não sabia o que fazer. Engraçado como agora as soluções parecem simples rs
