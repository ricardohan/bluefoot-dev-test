import React, { Component } from 'react';


class SearchBar extends Component {
  constructor(props) {
    super(props);
    // this.state = {term: ''};
  }

  render() {
    return (
     <div className="searchBar">
       <input
       value={this.props.term}
       onChange={(event) => this.onInputChange(event.target.value, false)} />
       <button onClick={() => this.onClickChange(true)}>Search</button>
     </div>
   );
  }

  onInputChange(term, boolean) {
    this.props.changeState(term);
    this.props.onSearchTermChange(term);
    this.props.changeDisplay(boolean);
  }

  onClickChange(boolean) {
    this.props.changeDisplay(boolean);
  }

};

export default SearchBar;
