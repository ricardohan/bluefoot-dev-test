import React, { Component } from 'react';
import SearchProductItem from './search_product_item';
import AutoCompleteList from './autocompleteList';

// changeState={this.props.changeState(term)} term={this.props.term}

class SearchWrapper extends Component {
  constructor(props) {
    super(props);
    };

  render() {
    const productItem = this.props.products.slice(0, 3).map((product) => {
      return <SearchProductItem key={product.productId} product={product} />
    });

    const completeWords = this.props.words.slice(0, 8).map((word) => {
      return (
        <AutoCompleteList key={word.name} word={word} changeState={this.props.changeState.bind(this)} term={this.props.term}/>
      )
    });



    return (
      <div className="search_container">
        <ul className="search_word_list">
          {completeWords}
        </ul>
        <ul className="search_product_list">
          {productItem}
        </ul>
      </div>
    )
  }


}

export default SearchWrapper;
